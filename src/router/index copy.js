import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'/'el-icon-x' the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
// 公共页面
export const constantRoutes = [
  // 登录
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    meta: { title: 'login', icon: '' },
    hidden: true
  },
  // 注册
  {
    path: '/register',
    component: () => import('@/views/login/register'),
    meta: { title: '', icon: '', noCache: true },
    hidden: true
  },
  
  // 首页
  {
    path: '/',
    component: Layout,
    redirect: '/index',
    meta: { title: '首页', icon: 'dashboard' },
    children: [
      {
        path: 'index',
        name: 'index',
        component: () => import('@/views/index/index'),
        meta: { title: '首页', icon: 'el-icon-s-home' }
      }
    ]
  },
  // 修改密码
  {
    path: '/ChangePassword',
    component: Layout,
    redirect: '/ChangePassword/index',
    alwaysShow: true,
    hidden: true,
    meta: { title: '修改密码', icon: 'el-icon-s-tools' },
    children: [{
      path: 'index',
      name: 'index',
      component: () => import('@/views/ChangePassword/index'),
      meta: { title: '修改密码', icon: '' }
    },]
  },
  // 新闻和公告
  {
    path: '/news',
    component: Layout,
    redirect: '/news/news_consult',
    alwaysShow: true,
    // 面包屑中不可点击
    redirect: 'noRedirect',
    meta: { title: '新闻公告', icon: 'el-icon-news' },
    children: [{
        path: 'news_consult',
        name: 'news_consult',
        redirect: 'noRedirect',
        component: () => import('@/views/newsAndNotice/index'),
        meta: { title: '新闻咨询', icon: '' },
        children: [{
          path: 'news_consult_index',
          name: 'news_consult_index',
          component: () => import('@/views/newsAndNotice/news/index'),
          meta: { title: '首页', icon: '' }
        },{
          path: 'add_article',
          name: 'add_article',
          hidden:true,
          component: () => import('@/views/newsAndNotice/news/add_article'),
          meta: { title: '发布新闻', icon: '', activeMenu: '/news/news_consult/news_consult_index' }
        },{
          path: 'news_consult__draft',
          name: 'news_consult__draft',
          component: () => import('@/views/newsAndNotice/news/draft'),
          meta: { title: '草稿箱', icon: '' }
        },{
          path: 'add_article_draft',
          name: 'add_article_draft',
          hidden:true,
          component: () => import('@/views/newsAndNotice/news/add_article'),
          meta: { title: '发布新闻', icon: '', activeMenu: '/news/news_consult/news_consult__draft' }
        },{
          path: 'newsinfo_details',
          name: 'newsinfo_details',
          hidden:true,
          component: () => import('@/views/newsAndNotice/details'),
          meta: { title: '详情', icon: '', activeMenu: '/news/news_consult/news_consult_index'  }
        },]
      },{
        path: 'Notice_push',
        name: 'Notice_push',
        redirect: 'noRedirect',
        component: () => import('@/views/newsAndNotice/index'),
        meta: { title: '公告推送', icon: '' },
        children: [{
          path: 'Notice_push_index',
          name: 'Notice_push_index',
          component: () => import('@/views/newsAndNotice/Notice_push/index'),
          meta: { title: '首页', icon: '' }
        },{
          path: 'Notice_push_add',
          name: 'Notice_push_add',
          hidden:true,
          component: () => import('@/views/newsAndNotice/Notice_push/add_article'),
          meta: { title: '发布公告', icon: '' , activeMenu: '/news/Notice_push/Notice_push_index' }
        },{
          path: 'Notice_push_draft',
          name: 'Notice_push_draft',
          component: () => import('@/views/newsAndNotice/Notice_push/draft'),
          meta: { title: '草稿箱', icon: '' }
        },{
          path: 'Notice_push_add_draft',
          name: 'Notice_push_add_draft',
          hidden:true,
          component: () => import('@/views/newsAndNotice/Notice_push/add_article'),
          meta: { title: '发布公告', icon: '' , activeMenu: '/news/Notice_push/Notice_push_draft' }
        },{
          path: 'Noticeinfo_details',
          name: 'Noticeinfo_details',
          hidden:true,
          component: () => import('@/views/newsAndNotice/details'),
          meta: { title: '详情', icon: '', activeMenu: '/news/Notice_push/Notice_push_index'  }
        },]
      },{
        path: 'info_approval',
        name: 'info_approval',
        component: () => import('@/views/newsAndNotice/info_approval/index'),
        meta: { title: '信息审批', icon: '' },
      },{
        path: 'info_approval_son',
        name: 'info_approval_son',
        hidden:true,
        component: () => import('@/views/newsAndNotice/info_approval/approval'),
        meta: { title: '信息审批详情', icon: '', activeMenu: '/news/info_approval'  }
      },{
        path: 'NewsAndNoticeInfo_manage',
        name: 'NewsAndNoticeInfo_manage',
        component: () => import('@/views/newsAndNotice/info_manage'),
        meta: { title: '信息撤回', icon: '' }
      },]
  } 
]
// 根据角色分权限的页面
export const asyncRoutes = [
  // 企业服务
  {
    path: '/admin',
    component: Layout,
    redirect: '/user',
    alwaysShow: true,
    meta: { title: '企业服务', icon: 'user' },
    children: [
      {
        path: 'user',
        name: 'CustomerMan',
        component: () => import('@/views/enterprise_service/customerMan'),
        meta: { title: '客户管理', icon: 'form' }
      },
      {
        path: 'menu',
        name: 'MenuMan',
        component: () => import('@/views/enterprise_service/menuMan'),
        meta: { title: '菜单管理', icon: 'form' }
      }
    ]
  }
]
// 公共页面--但是渲染的时候必须放在最后
export const notfound = [
  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },
  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]
const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
