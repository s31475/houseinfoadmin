export function getLable(element) {
  if (element.options.showTitle == undefined) {
    return element.name
  } else {
    if (element.options.showTitle) {
      return element.name
    } else {
      return ''
    }
  }
}
function S4() {
  return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1)
}
export function guid() {
  return (S4() + S4() + '-' + S4() + '-' + S4() + '-' + S4() + '-' + S4() + S4() + S4())
}
