import { login, getInfo } from '@/api/user'
import { getToken, setToken, removeToken } from '@/utils/auth'
import { resetRouter } from '@/router'

const getDefaultState = () => {
  return {
    token: getToken(),
    name: '',
    userObj: {}
  }
}

const state = getDefaultState()

const mutations = {
  RESET_STATE: (state) => {
    Object.assign(state, getDefaultState())
  },
  SET_TOKEN: (state, token) => {
    state.token = token
  },
  SET_NAME: (state, name) => {
    state.name = name
  },
  SET_USEROBJ: (state, user) => {
    state.userObj = user
  }
}

const actions = {
  // user login
  login({ commit }, userInfo) {
    const { username, password } = userInfo
    return new Promise((resolve, reject) => {
      login(userInfo).then(response => {
        const { data } = response
        console.log('login...', data);
        commit('SET_TOKEN', data)
        setToken(data)
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  },

  // get user info
  getInfo({ commit, state }) {
    return new Promise((resolve, reject) => {
      var token = getToken();
      if (token) {
        if (token) {
          try {
            getInfo(token).then(response => {
              const {data} = response

              if (!data) {
                reject('Verification failed, please Login again.')
              }

              const {roles, name, avatar, introduction, permissions} = data
              // roles must be a non-empty array
              if (!roles || roles.length <= 0) {
                reject('getInfo: roles must be a non-null array!')
              }
              commit('SET_ROLES', roles)
              if (permissions && permissions.length > 0) {
                setPermissionsToken(permissions)

              }
              commit('SET_NAME', name)
              commit('SET_AVATAR', avatar)
              commit('SET_INTRODUCTION', introduction)
              resolve(data)
            }).catch(error => {
              reject(error)
            })
          } catch (e) {

          }

        }
      }

    })
  },

  // user logout
  logout({ commit, state }) {
    return new Promise((resolve, reject) => {
      // logout(state.token).then(() => {
      removeToken() // must remove  token  first
      resetRouter()
      commit('RESET_STATE')
      resolve()
      // }).catch(error => {
      //   reject(error)
      // })
    })
  },

  // remove token
  resetToken({ commit }) {
    return new Promise(resolve => {
      commit('SET_TOKEN', '')
      removeToken() // must remove  token  first
      commit('RESET_STATE')
      resolve()
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

