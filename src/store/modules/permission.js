import { asyncRoutes, constantRoutes, notfound } from '@/router'

/**
 * Use meta.role to determine if the current user has permission
 * @param roles
 * @param route
 */
// function hasPermission(roles, route) {
//   if (route.meta && route.meta.roles) {
//     return roles.some(role => route.meta.roles.includes(role))
//   } else {
//     return true
//   }
// }

/**
 * Filter asynchronous routing tables by recursion
 * @param routes asyncRoutes
 * @param roles
 */
export function filterAsyncRoutes(routes, menus) {
  const res = []
  menus.forEach(item => {
    routes.forEach(route => {
      const tmp = { ...route }
      if (item.path === tmp.path) {
        tmp.meta.title = item.name
        if (item.children) {
          tmp.children = filterAsyncRoutes(tmp.children, item.children)
        }
        res.push(tmp)
      }
    })
  })
  res.concat(notfound)
  return res
}

const state = {
  routes: [],
  addRoutes: []
}

const mutations = {
  SET_ROUTES: (state, routes) => {
    console.log('SET_ROUTES', constantRoutes);
    state.addRoutes = routes
    state.routes = constantRoutes.concat(routes)
  }
}

const actions = {
  generateRoutes({ commit }) {
  // generateRoutes({ commit }, menus) {
    return new Promise(resolve => {
      // const accessedRoutes = filterAsyncRoutes(asyncRoutes, menus)
      commit('SET_ROUTES', asyncRoutes)
      resolve(asyncRoutes)
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
