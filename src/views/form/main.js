import Vue from 'vue'
import App from './App.vue'
import router from './router'
import ElementUI from 'element-ui'

import 'element-ui/lib/theme-chalk/index.css'
import iconPicker from 'vue-fontawesome-elementui-icon-picker';


import comm from './components/commonapi/common'
Vue.use(iconPicker);
// import Distpicker from 'v-distpicker'
//
// Vue.component('v-distpicker', Distpicker)
import Vue2Editor from "vue2-editor";

Vue.use(Vue2Editor);
import iView from 'view-design'
import 'view-design/dist/styles/iview.css'
Vue.use(iView)
Vue.config.productionTip = false

Vue.use(ElementUI, { size: 'small' })

var vm=new Vue({
  router,
  render: h => h(App),
  data:function () {
    return {
      jsExtend: []

    }
  }

}).$mount('#app')

export default vm;
