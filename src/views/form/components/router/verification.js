
import request from '../../util/request.js'

export function verification(rule, info, callback,obj) {
    var type= rule.customerType;
    var length= rule.val;

    // if (!info){
    //     callback();
    //     return
    // }
    if (type=="nonull" && info.length==0) {
        callback(new Error(rule.msg));
    }else if (type=="isnum" && !/^[0-9]*$/.test(info) && info.length>0) {
        callback(new Error(rule.msg));
    }else if (type=="min"  && info.length<length) {
        callback(new Error(rule.msg));
    }else if (type=="max"  && info.length>length) {
        callback(new Error(rule.msg));
    }
    else if (type=="phone"  && info.length>0 && !validatePhone(info)) {
        callback(new Error(rule.msg));
    }
    else if (type=="idNumber"  && info.length>0 && !validateIdNo(info)) {
        callback(new Error(rule.msg));
    }
    //调用接口
    else if (type=="connector"  ) {
        var connector= rule.connector;
        if (connector){
            request.get(connector+"/?val="+info ).then(res => {
               if (res.code==0){

               } else{
                   callback(new Error(rule.msg));
               }


            })

        }
    }
    else if (type=="email"  && info.length>0 && !validateEMail(info)) {
        callback(new Error(rule.msg));
    }
    else if (type=="url"  && info.length>0 && !validateURL(info)) {
        callback(new Error(rule.msg));
    }
    else if (type=="pwd"  && info.length>0 && !validatePwd(info)) {
        callback(new Error(rule.msg));
    }
    else if (type=="date"  && info.length>0 && !validateDate(info)) {
        callback(new Error(rule.msg));
    }
    //regular
    else if (type=="regular"  && info.length>0 && !validateReg(length,info)) {
        callback(new Error(rule.msg));
    }
    else if (rule.isregexp && !/^[^%'"?!^#%*/]*$/.test(info)) {
        callback(new Error(rule.msg));
    } else if (rule.isnumeng_ && !/^[0-9a-zA-Z_]*$/.test(info.value)) {
        callback(new Error(rule.msg));
    } else if (rule.maxlength && !info.value === "" && info.value.length > rule.maxlength) {
        callback(new Error(rule.msg));
    }  else if (type=="dateRule" ) {
        var currentVal=  obj.models[obj.widget.model]
        var otherVal=  obj.models[obj.widget.options.ckeckedDatevalidType]
        var field= rule.field
        var pathDot=          rule.field.split(".")

        if (pathDot) {
            var otherArray=[];
            var forceNode;
            var blockId = pathDot[0];
            var index = pathDot[1];
            var modelId = pathDot[2];
            var node = currentVal[index]
            var operateName = '';
            var vals=  currentVal[index];
            var dateValidRuleResult=false;
            var columns=[];
            if (obj.widget.tabColumns) {
                columns=obj.widget.tabColumns;
            }
            if (obj.widget.tableColumns) {
                columns=obj.widget.tableColumns;
            }
            for (var i = 0; i <columns[index].length ; i++) {
                var node= columns[index][i];
                if (node.model!=modelId) {
                    otherArray.push(node)
                }else{
                    forceNode=node;
                }
            }
            if (forceNode){
                //计算
                var dateValidRule= forceNode.options.dateValidRule
                var ckeckedDatevalidType= forceNode.options.ckeckedDatevalidType
               var vals=  obj.models[blockId][index];
              var otherVal=  vals[ckeckedDatevalidType]
              var currentVal=  vals[modelId]
                var operateName="";
                if (">="==dateValidRule){
                    operateName="大于等于";
                    dateValidRuleResult= (currentVal-otherVal>=0);
                } else if("<"==dateValidRule){
                    operateName="小于";
                    dateValidRuleResult=(currentVal-otherVal<0);
                } else if("=="==dateValidRule){
                    operateName="等于";
                    dateValidRuleResult= (currentVal-otherVal==0);
                }else if("<="==dateValidRule){
                    operateName="小于等于";
                    dateValidRuleResult= (currentVal-otherVal<=0);
                }
                if (!dateValidRuleResult) {
                    callback(new Error("日期值不符合要求"));
                }else{
                    callback();
                }


            }
        }
    }  else{
        callback();
    }

}
export function validatePhone(str) {
    var reg =/^[1][3-9][0-9]{9}$/;
    // var reg= new RegExp(regStr)
    return reg.test(str);
}
export function validateIdNo(str) {
    const reg = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
    return reg.test(str);
}

export function validateEMail(str) {
    const reg =/^([a-zA-Z0-9]+[-_\.]?)+@[a-zA-Z0-9]+\.[a-z]+$/;
    return reg.test(str);
}

export function validateURL(url) {
    const urlregex = /^(https?|ftp):\/\/([a-zA-Z0-9.-]+(:[a-zA-Z0-9.&%$-]+)*@)*((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9]?[0-9])){3}|([a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(:[0-9]+)*(\/($|[a-zA-Z0-9.,?'\\+&%$#=~_-]+))*$/;
    return urlregex.test(url);
}
export function validatePwd  (str){
    return /^(?![\d]+$)(?![a-zA-Z]+$)(?![^\da-zA-Z]+$)([^\u4e00-\u9fa5\s]){6,20}$/.test(str);
}


export  function validateDate(str) {
    const dateRegex =  /((^((1[8-9]\\d{2})|([2-9]\\d{3}))([-\\/\\._])(10|12|0?[13578])([-\\/\\._])(3[01]|[12][0-9]|0?[1-9])$)|(^((1[8-9]\\d{2})|([2-9]\\d{3}))([-\\/\\._])(11|0?[469])([-\\/\\._])(30|[12][0-9]|0?[1-9])$)|(^((1[8-9]\\d{2})|([2-9]\\d{3}))([-\\/\\._])(0?2)([-\\/\\._])(2[0-8]|1[0-9]|0?[1-9])$)|(^([2468][048]00)([-\\/\\._])(0?2)([-\\/\\._])(29)$)|(^([3579][26]00)([-\\/\\._])(0?2)([-\\/\\._])(29)$)|(^([1][89][0][48])([-\\/\\._])(0?2)([-\\/\\._])(29)$)|(^([2-9][0-9][0][48])([-\\/\\._])(0?2)([-\\/\\._])(29)$)|(^([1][89][2468][048])([-\\/\\._])(0?2)([-\\/\\._])(29)$)|(^([2-9][0-9][2468][048])([-\\/\\._])(0?2)([-\\/\\._])(29)$)|(^([1][89][13579][26])([-\\/\\._])(0?2)([-\\/\\._])(29)$)|(^([2-9][0-9][13579][26])([-\\/\\._])(0?2)([-\\/\\._])(29)$))/
    return dateRegex.test(str);

}
export function validateReg(reg,str) {
    var patt1=new RegExp(reg);
    return patt1.test(str);
}

