import FileSaver from "file-saver";
import XLSX from "xlsx";

export  function  importXLSX(datas,name) {
    const ws= XLSX.utils.aoa_to_sheet(datas);

    /* generate workbook and add the worksheet */
    const wb = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, name+'.xlsx');
}