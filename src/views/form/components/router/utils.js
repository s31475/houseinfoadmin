export function getAllControls(list, model, otherNodes) {
    if (!otherNodes) {
        var otherNodes = [];
    }

    for (var i = 0; i < list.length; i++) {
        var node = list[i];
        if (node.type != 'time' && node.type != 'date') {
            if (node.tableColumns) {
                //  getAllControls(node.tableColumns,model,otherNodes);
            }
            if (node.options.dateCalculation) {
                //参与计算
                if (node.options.dateCalculationStart == model || node.options.dateCalculationEnd == model) {
                    otherNodes.push(node)
                }
            }

        }
    }
    return otherNodes

}

export function getDateControls(list, model, otherNodes, parent) {

    if (!otherNodes) {
        var otherNodes = [];
    }
    var findModel = filterByModel(list, model);
    if (findModel.length > 0) {
        //一级
        return findAllModel(list);
    } else {
        return findTableColumns(list, model);
    }
    return otherNodes

}

export function findTableColumns(list, model) {
    var otherNodes = [];
    for (var i = 0; i < list.length; i++) {
        var node = list[i];
        if (node.tableColumns) {

            var findModel = filterByModel(node.tableColumns, model)
            if (findModel.length == 0) {
                if (node.tableColumns instanceof Array) {
                    //tab
                    for (var j = 0; j < node.tableColumns.length; j++) {
                        var subNode = node.tableColumns[j]

                        findModel = filterByModel(subNode, model);
                        if (findModel.length > 0) {
                            otherNodes = findAllModel(subNode);
                            break
                        }


                    }
                }
            } else if (findModel.length > 0) {
                otherNodes = findAllModel(node.tableColumns);
                break;
            }

        } else {
            if (node.tabColumns instanceof Array) {
                //tab
                for (var j = 0; j < node.tabColumns.length; j++) {
                    var subNode = node.tabColumns[j]

                    findModel = filterByModel(subNode, model);
                    if (findModel.length > 0) {
                        otherNodes = findAllModel(subNode);
                        break
                    }


                }
            }
        }
    }
    return otherNodes;
}

function findAllModel(list) {
    var otherNodes = [];
    for (var i = 0; i < list.length; i++) {
        var node = list[i];
        if (node.tableColumns) {
        }
        if (node.type == 'time' || node.type == 'date') {
            otherNodes.push(node)
        }
    }
    return otherNodes;
}

function findAllNotDateModel(list) {
    var otherNodes = [];
    for (var i = 0; i < list.length; i++) {
        var node = list[i];
        if (node.tableColumns) {
        }
        if (node.type != 'date') {
            otherNodes.push(node)
        }
    }
    return otherNodes;
}

export function findTableColumnControls(list, model, index) {
    for (var i = 0; i < list.length; i++) {
        var node = list[i];
        if ((node.type == 'block' || node.type == 'card') && node.model == model) {
            if (node.tableColumns) {
                var subProcess = node.tableColumns;
                return findAllNotDateModel(subProcess)
            }
        } else if (("tabs" == node.type || "collapse" == node.type) && node.model == model) {
            if (node.tabColumns) {
                var subProcess = node.tabColumns;
                if (subProcess) {
                    var subNode = subProcess[index];
                    return findAllNotDateModel(subNode)
                }


            }
        }
    }
}

export function dataControls(list, model) {
    for (var i = 0; i < list.length; i++) {
        var node = list[i];
        if (node.type == 'block' && node.model == model) {
            if (node.tableColumns) {
                var subProcess = node.tableColumns;
                return findAllNotDateModel(subProcess)
            }


        }
    }
}

// 根据单个名字筛选
function filterByModel(list, model) {
    if (list) {
        return list.filter(item => item.model == model)
    }

}

export function filterModel(list, model) {
    if (list) {
        return list.filter(item => item.model == model)
    }

}

export function uniqueUser(users) {
    let deWeightThree = () => {
        let map = new Map();
        for (let item of users) {
            if (!map.has(item.key)) {
                map.set(item.key, item);
            }
        }
        return [...map.values()];
    }

    let res = deWeightThree();

    return res;

}

export function isRepeat(arr) {

    var hash = {};

    for (var i in arr) {

        if (hash[arr[i]])

            return arr[i];

        hash[arr[i]] = true;

    }

    return false;

}
export function genID(length){
    return Number(Math.random().toString().substr(3,length) + Date.now()).toString(36);
}

