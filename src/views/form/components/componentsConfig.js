export const basicComponents = [   // 默认表单组成的配置
  {
    type: 'input',
    name: '单行',
    controlType:'',
    icon: 'icon-input',
    options: {
      show: true,
      showTitle: true,
      showAnnotation:false,
      annotationTitle:'添加批注',
      annotationContext:'',
      unit:'',
      dateCalculation:false,
      showPassword:false,
      dateCalculationStart:'',
      dateCalculationEnd:'',
      dateCalcDiffType:'day',
      width: '70%',
      defaultValue: '',
      required: false,
      dataType: 'string',
      span: 12,
      pattern: '',
      placeholder: '',
      customClass: '',
      customStyle: '',
      disabled: false,
      showRed: false,
    }
  },
  {
    type: 'textarea',
    name: '多行',
    controlType:'',
    icon: 'icon-diy-com-textarea',
    options: {
      dataType: 'string',
      showAnnotation:false,
      annotationTitle:'添加批注',
      annotationContext:'',
      show: true,
      showTitle: true,
      dateCalculation:false,
      dateCalculationStart:'',
      dateCalculationEnd:'',
      dateCalcDiffType:'day',
      width: '70%',
      defaultValue: '',
      span: 12,
      required: false,
      disabled: false,
      pattern: '',
      placeholder: '',
      customClass: '',
      showRed: false,
    }
  },
  {
    type: 'select',
    name: '下拉选择框',
    icon: 'icon-select',
    controlType:'',
    options: {
      showAnnotation:false,
      annotationTitle:'添加批注',
      annotationContext:'',
      show: true,
      showTitle: true,
      defaultValue: '',
      dateCalculation:false,
      dateCalcDiffType:'day',
      dateCalculationStart:'',
      dateCalculationEnd:'',
      span: 12,
      multiple: false,
      disabled: false,
      clearable: false,
      placeholder: '',
      required: false,
      showLabel: false,
      width: '',
      options: [
        {
          value: '下拉框1'
        },
        {
          value: '下拉框2'
        }
      ],
      effect:[],
      remote: false,
      filterable: false,
      remoteOptions: [],
      props: {
        value: '',
        label: ''
      },
      remoteFunc: '',
      remoteUrlKey: '',
      customClass: '',
      showRed: false,

    }
  },

  {
    type: 'radio',
    name: '单选框',
    controlType:'',
    dateCalcDiffType:'day',
    icon: 'icon-radio-active',
    options: {
      showAnnotation:false,
      annotationTitle:'添加批注',
      annotationContext:'',
      show: true,
      showTitle: true,
      inline: true,
      defaultValue: '',
      showLabel: false,
      span: 12,
      controlType:'',
      dateCalculation:false,
      dateCalcDiffType:'day',
      dateCalculationStart:'',
      dateCalculationEnd:'',

      options: [
        {
          value: '选项1',
          label: '选项1'
        },
        {
          value: '选项2',
          label: '选项2'
        }
      ],
      effect:[],
      required: false,
      border:false,
      disabled:false,
      width: '',
      remote: false,
      remoteOptions: [],
      remoteUrlKey: '',
      props: {
        value: 'value',
        label: 'label'
      },
      remoteFunc: '',
      customClass: '',
      showRed: false,

    }
  },
  {
    type: 'checkbox',
    name: '复选框',
    icon: 'icon-check-box',
    options: {
      showAnnotation:false,
      annotationTitle:'添加批注',
      annotationContext:'',
      show: true,
      showTitle: true,
      span: 12,
      inline: true,
      disabled:false,
      dateCalculation:false,
      dateCalcDiffType:'day',
      dateCalculationStart:'',
      dateCalculationEnd:'',
      effect:[],
      defaultValue: [],
      defaultValues: [],
      showLabel: false,
      options: [
        {
          value: '选项1'
        },
        {
          value: '选项2'
        }
      ],
      required: false,
      width: '',
      remote: false,
      remoteOptions: [],
      props: {
        value: 'value',
        label: 'label'
      },
      remoteUrlKey: '',
      remoteFunc: '',
      customClass: '',
    }
  },
  {
    type: 'date',
    name: '日期选择器',
    icon: 'icon-date',
    controlType:'',
    options: {
      showAnnotation:false,
      annotationTitle:'添加批注',
      annotationContext:'',
      show: true,
      showTitle: true,
      defaultValue: '',
      dataType: 'string',
      dateValidRule:'',
      ckeckedDatevalidType:'',
      readonly: false,
      span: 12,
      disabled: false,
      editable: true,
      clearable: true,
      placeholder: '',
      startPlaceholder: '',
      endPlaceholder: '',
      type: 'datetime',
      format: 'yyyy-MM-dd HH:mm:ss',
      'value-format': 'yyyy-MM-dd HH:mm:ss',
      timestamp: true,
      required: false,
      width: '',
      customClass: '',
      showRed: false,

    }
  },

  // {
  //   type: 'time',
  //   name: '时间选择器',
  //   icon: 'icon-time',
  //   controlType:'',
  //   dateCalcDiffType:'day',
  //   options: {
  //     show: true,
  //     defaultValue: '21:19:56',
  //     span: 12,
  //     readonly: false,
  //     disabled: false,
  //     editable: true,
  //     clearable: true,
  //     placeholder: '',
  //     startPlaceholder: '',
  //     endPlaceholder: '',
  //     rangeAble: false,
  //     arrowControl: true,
  //     format: 'HH:mm:ss',
  //     required: false,
  //     width: '',
  //     customClass: '',
  //     showRed: false,
  //
  //   }
  // },
  {
    type: 'fileUpload',
    name: '文件上传',
    icon: 'icon-shangchuan',
    options: {
      showAnnotation:false,
      annotationTitle:'添加批注',
      annotationContext:'',
      show: true,
      showTitle: true,
      sizes: {
        width: 100,
        height: 100,
      },
      width: 100,
      span: 12,
      token: '',
      disabled: false,
      length: 8,
      multiple: false,
      supportedAllTypes:["jpg","jpeg","png","bmp","pdf","PBG","BMP","doc","docx","xls","xlsx","ppt","pptx","rtf","txt","zip","rar","vsd","dwg"],
      supportedTypes:["jpg","jpeg","png","bmp","pdf","PBG","BMP","doc","docx","xls","xlsx","ppt","pptx","rtf","txt","zip","rar","vsd","dwg"],
      supportedTypeStr:"",

      min: 0,
      editable: false,
      action: 'http://127.0.0.1:8080/fastflow-admin/a1bpmn/api/multiUpload',
      customClass: '',
      showRed: false
    },
  },
  // {
  //   type: 'imgupload',
  //   name: '图片墙',
  //   icon: 'icon-tupian',
  //   options: {
  //     defaultValues: [],
  //     show: true,
  //     showTitle: true,
  //     size: {
  //       width: 100,
  //       height: 100,
  //     },
  //     span: 24,
  //     token: '',
  //     supportedAllTypes:["jpg","jpeg","png","bmp","PBG","BMP"],
  //     supportedTypes:   ["jpg","jpeg","png","bmp","PBG","BMP"],
  //     disabled: false,
  //     length: 8,
  //     multiple: false,
  //     isQiniu: false,
  //     isDelete: false,
  //     min: 0,
  //     isEdit: false,
  //     action: 'http://127.0.0.1:8080/fastflow-admin/a1bpmn/api/multiUpload/',
  //     customClass: '',
  //     showRed: false,
  //
  //   }
  //
  // },

  // {
  //   type: 'subTable',
  //   name: '子表',
  //   icon: 'icon-table',
  //   columns: [
  //     {
  //       span: 12,
  //       list: []
  //     },
  //     {
  //       span: 12,
  //       list: []
  //     }
  //   ],
  //   options: {
  //     gutter: 0,
  //     justify: 'start',
  //     align: 'top',
  //     customClass: '',
	 //  subTableName:'子表名称'
  //   }
  // },
  //
  // {
  //   type: 'processInfo',
  //   name: '审批记录',
  //   icon: 'icon-tupian',
  //   options: {
  //     showAnnotation:false,
  //     annotationTitle:'添加批注',
  //     annotationContext:'',
  //     show: true,
  //     showTitle: true,
  //     span: 24,
  //   },
  // },
  {
    type: 'iframe',
    name: 'iframe标签',
    icon: 'icon-fuwenbenkuang',
    options: {
      show: true,
      showTitle: true,
      src:"",
      width:'100%',
      span: 24,
      height:100,
      disabled: false
    },
  }

  ,


  {
    type: 'dialog',
    name: '对话框',
    icon: 'icon-ad-icon-tooltip',
    controlType:'',
    options: {
      showAnnotation:false,
      annotationTitle:'添加批注',
      annotationContext:'',
      show: true,
      showTitle: true,
      unit:'',
      chooseFiledList:[],
      customDialogAlias:'',
      btnName:'请选择',
      icon:'',
      dateCalculation:false,
      dateCalculationStart:'',
      dateCalculationEnd:'',
      dateCalcDiffType:'day',
      defaultValue: '',
      required: false,
      span: 12,
      pattern: '',
      customClass: '',
      disabled: false,
      showRed: false,
    }
  },
  {
    type: 'userDialog',
    name: '人员选择器',
    icon: 'icon-diy-com-textarea',
    controlType:'',
    options: {
      showAnnotation:false,
      annotationTitle:'添加批注',
      annotationContext:'',
      show: true,
      showTitle: true,
      unit:'',
      chooseFiledList:[],
      customDialogAlias:'',
      selectorType:'',
      btnName:'选择',
      icon:'',
      dateCalculation:false,
      isSingle:false,
      dateCalculationStart:'',
      dateCalculationEnd:'',
      dateCalcDiffType:'',
      defaultValue: '',
      required: false,
      span: 12,
      pattern: '',
      customClass: '',
      disabled: false,
      showRed: false,
    }
  },



  {
    type: 'badge',
    name: '标记',
    icon: 'icon-ic',
    controlType:'',
    options: {
      showAnnotation:false,
      annotationTitle:'添加批注',
      annotationContext:'',
      span: 12,
      defaultValue: '标记',
      show:true,
      showTitle: true,
      dot:false,
      max:10,
      disabled: false,
    },

  },

  {
    type: 'inputNumber',
    name: '计数器',
    icon: 'icon-input',
    controlType:'',
    options: {
      showAnnotation:false,
      annotationTitle:'添加批注',
      annotationContext:'',
      span: 12,
      defaultValue: 0,
      show:true,
      showTitle: true,
      max:0,
      min:0,
      step:1,
      radioSize:'small',
      disabled: false,
    },

  },
  {
    type: 'distpicker',
    name: '省市区级联',
    icon: 'icon-jilianxuanze',
    options: {
      showAnnotation:false,
      annotationTitle:'添加批注',
      annotationContext:'',
      show: true,
      showTitle: true,
      customClass: '',
      disabled: false,
      span: 24,
      distpicker:{
        province:"省",
        city:"市",
        area:"区",
      }
    }
  },
  {
    type: 'btn',
    name: '按钮',
    icon: 'icon-jilianxuanze',
    options: {
      radioSize:'small',
      type:'primary',
      name:'确定',
      showAnnotation:false,
      annotationTitle:'添加批注',
      annotationContext:'',
      show: true,
      showTitle: false,
      customClass: '',
      disabled: false,
      plain: false,
      round: false,
      circle: false,
      loading: false,
      jsExpand: '',
      icon: '',
      marginLeft: '',
      span: 12,
    }
  },
  // {
  //   type: 'divider',
  //   name: '分割线',
  //   icon: 'icon-fengexian',
  //   controlType:'',
  //   span: 24,
  //   options: {}
  // }
]

export const advanceComponents = [
  // {
  //   type: 'blank',
  //   name: '自定义',
  //   icon: 'icon-ic',
  //   options: {
  //     defaultType: 'String',
  //     customClass: '',
  //     width: ''
  //     ,showRed: false,
  //
  //   }
  // },

  // {
  //   type: 'editor',
  //   name: '富文本',
  //   icon: 'icon-fuwenbenkuang',
  //   options: {
  //     showAnnotation:false,
  //     annotationTitle:'添加批注',
  //     annotationContext:'',
  //     show: true,
  //     showTitle: true,
  //     defaultValue: '',
  //     width: '',
  //     span: 24,
  //     customClass: '',
  //     showRed: false,
  //     disabled: false,
  //
  //   }
  // },
  {
    type: 'vue2Editor',
    name: '富文本',
    icon: 'icon-fuwenbenkuang',
    options: {
      showAnnotation:false,
      annotationTitle:'添加批注',
      annotationContext:'',
      show: true,
      showTitle: true,
      defaultValue: '',
      span: 24,
      customClass: '',
      action: '',
      disabled: false,

    }
  },


  {
    type: 'table',
    name: '表格',
    icon: 'icon-table',
    options: {
      show: true,
      showTitle: true,
      defaultValues: [],
      customClass: '',
      span: 24,
      disabled: false,

    },
    tableColumns: []
  },
  {
    type: 'block',
    name: '区域',
    icon: 'icon-ic',
    options: {
      showAnnotation:false,
      annotationTitle:'添加批注',
      annotationContext:'',
      show: true,
      showTitle: true,
      defaultType: [],
      customClass: '',
       span: 24,
      disabled: false,

    },
	  tableColumns: []
  },
  {
    type: 'card',
    name: '卡片',
    icon: 'icon-icon_clone',
    options: {
      showAnnotation:false,
      annotationTitle:'添加批注',
      annotationContext:'',
      show: true,
      showTitle: true,
      defaultType: [],
      customClass: '',
      span: 24,
      shadow: "always",
      header: "",
      disabled: false,

    },
    tableColumns: []
  },
  {
    type: 'collapse',
    name: '折叠面板',
    icon: 'icon-icon_clone',
    options: {
      showAnnotation:false,
      annotationTitle:'添加批注',
      annotationContext:'',
      show: true,
      showTitle: true,
      defaultType: [],
      customClass: '',
      span: 24,
      accordion:false,
      disabled: false,

      options: [
        {
          value: '面板1',
          label: '面板1'
        },
        {
          value: '面板2',
          label: '面板2'
        }
      ],
    },
    tabColumns: [[],[]]
  },


  // {
  //   type: 'div',
  //   name: '布局',
  //   icon: 'icon-grid-',
  //   columns: [
  //     {
  //       span: 12,
  //       list: []
  //     }
  //   ],
  //   options: {
  //     span: 12,
  //   },
  //   tableColumns: []
  // }
]

export const layoutComponents = [
  {
    type: 'grid',
    name: '栅格布局',
    icon: 'icon-grid-',
    columns: [
      {
        span: 12,
        list: []
      },
      {
        span: 12,
        list: []
      }
    ],
    options: {
      gutter: 0,
      justify: 'start',
      align: 'top',
      customClass: '',
      span: 24,
    }
  },
  {
    type: 'rate',
    name: '评分',
    icon: 'icon-icon-test',
    controlType:'',
    options: {
      showAnnotation:false,
      annotationTitle:'添加批注',
      annotationContext:'',
      show: true,
      showTitle: true,
      defaultValue: null,
      max: 5,
      span: 12,
      disabled: false,
      allowHalf: false,
      required: false,
      customClass: '',
      showRed: false,

    }
  },
  {
    type: 'tag',
    name: '标签',
    icon: 'icon-icon-test',
    controlType:'',
    options: {
      showAnnotation:false,
      annotationTitle:'添加批注',
      annotationContext:'',
      show: true,
      showTitle: true,
      span: 12,
      defaultValue: '',
      radioSize:'small',
      options: [
        {
          value: 'success',
          label: '标签一'
        }
        ,
        {
          value: 'info',
          label: '标签二'
        }
        ,
        {
          value: 'warning',
          label: '标签三'
        }
        ,
        {
          value: 'danger',
          label: '标签四'
        }
      ],

    }
  },
  {
    type: 'color',
    name: '颜色选择器',
    icon: 'icon-color',
    controlType:'',
    options: {
      showAnnotation:false,
      annotationTitle:'添加批注',
      annotationContext:'',
      show: true,
      showTitle: true,
      defaultValue: '',
      span: 12,
      disabled: false,
      showAlpha: false,
      required: false,
      customClass: '',
      showRed: false,

    }
  },

  {
    type: 'switch',
    name: '开关',
    icon: 'icon-switch',
    controlType:'',
    options: {
      showAnnotation:false,
      annotationTitle:'添加批注',
      annotationContext:'',
      show: true,
      showTitle: true,
      defaultValue: false,
      span: 12,
      required: false,
      disabled: false,
      customClass: '',
    }
  }
  ,
  {
    type: 'slider',
    name: '滑块',
    icon: 'icon-slider',
    controlType:'',
    options: {
      showAnnotation:false,
      annotationTitle:'添加批注',
      annotationContext:'',
      show: true,
      showTitle: true,
      defaultValue: 0,
      disabled: false,
      span: 12,
      required: false,
      showTooltip: true,
      min: 0,
      max: 100,
      step: 1,
      showInput: false,
      range: false,
      width: '',
      customClass: '',
      showRed: false,

    }
  }
  ,
  {
    type: 'link',
    name: '文字链接',
    icon: 'icon-wenzishezhi-',
    controlType:'',
    options: {
      showAnnotation:false,
      annotationTitle:'添加批注',
      annotationContext:'',
      span: 12,
      show: true,
      showTitle: true,
      underline: false,
      href:"",
      defaultValue:"默认链接",
      type:"primary",
      target:"_blank",

    }
  }
  ,
  {
    type: 'progress',
    name: '进度条',
    icon: 'icon-icon_bars',
    controlType:'',
    options: {
      showAnnotation:false,
      annotationTitle:'添加批注',
      annotationContext:'',
      span: 12,
      percentage: 50,
      strokeWidth: 6,
      progressType: "line",
      status: "success",
      showText	: false,
      show: true,
      showTitle: true,
      textInside: true,


    }
  }
  ,
  {
    type: 'dateCalendar',
    name: '日历',
    icon: 'icon-date',
    controlType:'',
    options: {
      showAnnotation:false,
      annotationTitle:'添加批注',
      annotationContext:'',
      span: 24,
      show: true,
      showTitle: true,
      disabled: false,
    }
  }
  ,
  {
    type: 'dividers',
    name: '分割线',
    icon: 'icon-fengexian',
    controlType:'',
    options: {
      showAnnotation:false,
      annotationTitle:'添加批注',
      annotationContext:'',
      span: 24,
      show: true,
      showTitle: true,
    }
  }
  ,
  {
    type: 'alert',
    name: '警告',
    icon: 'icon-ad-icon-tooltip',
    controlType:'',
    options: {
      showAnnotation:false,
      annotationTitle:'添加批注',
      annotationContext:'',
      span: 24,
      show: true,
      showTitle: true,
      title: "",
      defaultValue:"警告",
      alertType: "info",
      description: "",
      closeText: "",
      item: "light",
      closable: true,
      center: true,
      showIcon: true,
    }
  }
]
export const navigationComponents = [
  {
    type: 'tabs',
    name: '选项卡',
    icon: 'icon-icon_clone',
    controlType:'',
    options: {
      showAnnotation:false,
      annotationTitle:'添加批注',
      annotationContext:'',
      show: true,
      showTitle: true,
      defaultType: [],
      customClass: '',
      activeName: '标签页1',
      stretch: false,
      tabsType: "",
      tabPosition: "top",
      span: 24,
      options: [
        {
          value: '标签页1',
          label: '标签页1'
        },
        {
          value: '标签页2',
          label: '标签页2'
        }
      ],
    },
    tabColumns: [[],[]]

  },
  {
    type: 'breadcrumb',
    name: '面包屑',
    icon: 'icon-grid-',
    controlType:'',
    options: {
      showAnnotation:false,
      annotationTitle:'添加批注',
      annotationContext:'',
      span: 24,
      show: true,
      showTitle: true,
      defaultValue:"面包屑",
      separator:"/",
      breadcrumbItemOptions: [
        {
          value: '首页',
          replace: false
        },
        {
          value: '活动页',
          replace: false
        }
      ],
    }
  },
  {
    type: 'steps',
    name: '步骤条',
    icon: 'icon-slider',
    controlType:'',
    options: {
      showAnnotation:false,
      annotationTitle:'添加批注',
      annotationContext:'',
      span: 24,
      show: true,
      showTitle: true,
      defaultValue:0,
      active:0,
      nextStep:false,
      alignCenter:false,
      finishStatus:"success",
      processStatus:"success",
      direction:"horizontal",
      space:0,
      simple:false,
      stepItemOptions: [
        {
          value: '步骤1',
          description: ""
        },
        {
          value: '步骤2',
          description: ""
        },
        {
          value: '步骤3',
          description: ""
        }
      ],
    }
  },

]
export const uncommonlyUsedComponents = [
  {
    type: 'h4',
    name: '标题',
    icon: 'fa fa-header',
    controlType:'',
    options: {
      showAnnotation:false,
      annotationTitle:'添加批注',
      annotationContext:'',
      span: 24,
      show: true,
      showTitle: false,
      defaultValue:"标题",
    }
  },
  {
    type: 'text',
    name: '文本',
    icon: 'fa fa-chain-broken',
    controlType:'',
    options: {
      showAnnotation:false,
      annotationTitle:'添加批注',
      annotationContext:'',
      dateCalculation:false,

      dateCalculationStart:'',
      dateCalculationEnd:'',
      show: true,
      showTitle: false,
      defaultValue: '文本',
      showRed: false,
      customClass: '',
      color: '',
      tip: '',
      fontSize: 12,
      span: 12,
      bold:false


    },

  },
  {
    type: 'p',
    name: '段落',
    icon: 'fa fa-product-hunt',
    controlType:'',
    options: {
      showAnnotation:false,
      annotationTitle:'添加批注',
      annotationContext:'',
      span: 24,
      show: true,
      showTitle: false,
      defaultValue:"段落",
    }
  },
  {
    type: 'small',
    name: '小字',
    icon: 'fa fa-font',
    controlType:'',
    options: {
      showAnnotation:false,
      annotationTitle:'添加批注',
      annotationContext:'',
      span: 24,
      show: true,
      showTitle: false,
      defaultValue:"小字",
    }
  },
  {
    type: 'mark',
    name: '高亮',
    icon: 'fa fa-bold',
    controlType:'',
    options: {
      showAnnotation:false,
      annotationTitle:'添加批注',
      annotationContext:'',
      span: 24,
      show: true,
      showTitle: false,
      defaultValue:"高亮",
    }
  },
  {
    type: 'del',
    name: '中划线',
    icon: 'fa fa-strikethrough',
    controlType:'',
    options: {
      showAnnotation:false,
      annotationTitle:'添加批注',
      annotationContext:'',
      span: 24,
      show: true,
      showTitle: false,
      defaultValue:"中划线",
    }
  },

  {
    type: 'u',
    name: '下划线',
    icon: 'fa fa-underline',
    controlType:'',
    options: {
      showAnnotation:false,
      annotationTitle:'添加批注',
      annotationContext:'',
      span: 24,
      show: true,
      showTitle: false,
      defaultValue:"下划线",
    }
  },
  {
    type: 'em',
    name: '斜体',
    icon: 'fa fa-italic',
    controlType:'',
    options: {
      showAnnotation:false,
      annotationTitle:'添加批注',
      annotationContext:'',
      span: 24,
      show: true,
      showTitle: false,
      defaultValue:"斜体",
    }
  },
  {
    type: 'strong',
    name: '加粗',
    icon: 'fa fa-bold',
    controlType:'',
    options: {
      showAnnotation:false,
      annotationTitle:'添加批注',
      annotationContext:'',
      span: 24,
      show: true,
      showTitle: false,
      defaultValue:"加粗",
    }
  },
  {
    type: 'icon',
    name: '图标',
    icon: 'fa fa-cog',
    controlType:'',
    options: {
      showAnnotation:false,
      annotationTitle:'添加批注',
      annotationContext:'',
      span: 24,
      show: true,
      showTitle: false,
      defaultValue:"",
      icon:"fa fa-cog",
    }
  },
]
