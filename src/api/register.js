
import request from '@/utils/request'

export function register(data) {
    return request({
      url: '/api-user/open',
      method: 'post',
      data
    })
}
// 查找园区名称
export function getParkNameList() {
  return request({
    url: '/api-user/tbPark/name',
    method: 'get'
  })
}