import request from '@/utils/request2'
import {Login,userInfo,GET_RESOURCE3,SERVICE_ADMIN,SERVICE_BPM} from '../api'

// 查询V4定义模型列表
export function listModel(query) {
  return request({
    url: SERVICE_BPM+'/biz/model/list',
    method: 'get',
    params: query
  })
}

// 查询V4定义模型详细
export function getModel(id) {
  return request({
    url: SERVICE_BPM+'/biz/model/' + id,
    method: 'get'
  })
}
export function transformationModel(id) {
  return request({
    url: SERVICE_BPM+'/biz/model/transformation/' + id,
    method: 'get'
  })
}
export function publishModel(id) {
  return request({
    url: SERVICE_BPM+'/biz/model/repository/process-definitions/publish/' + id,
    method: 'get'
  })
}

// 新增V4定义模型
export function addModel(data) {
  return request({
    url: SERVICE_BPM+'/biz/model',
    method: 'post',
    data: data
  })
}

// 修改V4定义模型
export function updateModel(data) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_BPM+'/biz/model/edit',
    method: 'POST',
    data: JSON.stringify(data)
  })
}

// 删除V4定义模型
export function delModel(id) {
  return request({
    url:SERVICE_BPM+ '/biz/model/remove/' + id,
    method: 'GET'
  })
}

// 导出V4定义模型
export function exportModel(query) {
  return request({
    url: SERVICE_BPM+'/biz/model/export',
    method: 'get',
    params: query
  })
}
export function getDeployModel(query) {
  return request({
    url: SERVICE_BPM+'/biz/model/getDeployModel',
    method: 'get',
    params: query
  })
}
