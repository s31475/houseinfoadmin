import request from '@/utils/request2'
import { Login, userInfo, GET_RESOURCE3, SERVICE_ADMIN, SERVICE_BPM } from './api'

// 查询流程按钮列表
export function listBtn(query) {
  return request({
    url: SERVICE_BPM + '/a1bpmn/api/btn/List',
    method: 'POST',
    params: query
  })
}

// 查询流程按钮详细
export function getBtn(id) {
  return request({
    url: '/flow/btn/' + id,
    method: 'get'
  })
}

// 新增流程按钮
export function addBtn(data) {
  return request({
    url: '/flow/btn',
    method: 'post',
    data: data
  })
}

// 修改流程按钮
export function updateBtn(data) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: '/flow/btn/edit',
    method: 'POST',
    data: JSON.stringify(data)
  })
}

// 删除流程按钮
export function delBtn(id) {
  return request({
    url: '/flow/btn/remove/' + id,
    method: 'GET'
  })
}

// 导出流程按钮
export function exportBtn(query) {
  return request({
    url: '/flow/btn/export',
    method: 'get',
    params: query
  })
}
