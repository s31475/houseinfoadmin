import request from '@/utils/request'
import qs from 'qs'
import { deletefn, deletefn1, get, put, post } from '@/utils/request'
import {Login,userInfo,GET_RESOURCE3,SERVICE_ADMIN,SERVICE_FORM,SERVICE_BPM} from './api'

export function getOutgoingFlows(id) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM+'/a1bpmn/api/cockpit/process-instance/outgoingFlows/' + id,
    method: 'get'
  })
}

export function getNextNode(taskId) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url:SERVICE_BPM+ '/a1bpmn/api/cockpit/process-instance/getNextNode/' + taskId,
    method: 'get'
  })
}

export function completeTask(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM+'/a1bpmn/api/runtime/task/v2/complete',
    method: 'POST',
    data: qs.stringify(data)
  })
}

export function getTaskBtn(taskId) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url:SERVICE_BPM+ '/a1bpmn/api/runtime/getTaskBtn/' + taskId,
    method: 'get'
  })
}

/**
 * 历史意见
 * @param id
 */
 export function opinionTask(id) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_BPM+'/a1bpmn/api/runtime/hisInstance/v1/nodeOpinion?instId=' + id,
    method: 'get'
  })
}

export function getTbCheckHousingByPG(data) {
  // console.log('getTbCheckHousing', data, page, limit);
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: `/pangu-admin/a1bpmn/api/runtime/task/v2/list`,
    method: 'post',
    data: qs.stringify(data)
  })
}

export function renderByTaskId(id) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_FORM + '/form/bus/render/get/' + id,
    method: 'get'
  })
}

export function testPanguBpm(data) {
  return request({
      url: SERVICE_BPM + '/a1bpmn/api/runtime/instance/v2/start',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      method: 'POST',
      data: qs.stringify(data)
  })
}

// 房源录入-------------------------------------------
export function getHouseInputInfo(params, page, limit, plotName, unit) {
    return get('/tbPlotHousing/select', {
        params,
        page,
        limit, plotName, unit
    })
}
// 单条房屋录入信息
export function getHouseInputInfoById(id) {
    return get('/tbPlotHousing/selectPlot', {
        id
    })
}
// 修改信息
export function putHouseInputInfo(data) {
    return request({
        url: '/tbPlotHousing/update',
        method: 'put',
        data
    })
}
// 添加
export function addHouseInputInfo(data) {
    return request({
        url: '/tbPlotHousing/save',
        method: 'post',
        data
    })
}
// 删除
export function delHouseInputInfo(idList) {
    return deletefn1('/tbPlotHousing/delete', {
        idList
    })
}

export function selectplot(page, limit) {
  return get('/tbPlotHousing/select1', {
    page, limit
  })
}

export function selectHouse(page, limit) {
  return get('/tbPlotHousing/select2', {
    page, limit
  })
}
// 台账查册
// /tbTenement
/**
 * 自有物业台账
 */
export function getYears() {
  return get('/tbTenement/year')
}

export function getTbTenement(enteringTime) {
  return get('/tbTenement', {
    enteringTime
  })
}

export function addTbTenement(data) {
  return request({
    url: '/tbTenement/add',
    method: 'post',
    data
  })
}

export function updateTbTenement(data) {
  return request({
    url: '/tbTenement',
    method: 'put',
    data
  })
}

export function deleteTbTenement(idList) {
  return delete('/tbTenement', {
    idList
  })
}

export function selectTbTenement(params, page, limit, enteringTime, purpose) {
  return get('/tbTenement/select', {
    params, page, limit, enteringTime, purpose
  })
}


/**
 * 小区物业台账
 */


 export function getTbRegion(enteringTime) {
  return get('/tbRegion', {
    enteringTime
  })
}


export function addTbRegion(data) {
  return request({
    url: '/tbRegion',
    method: 'post',
    data
  })
}

export function selectTbRegion(params, page, limit, enteringTime, attribute) {
  return get('/tbRegion/selectXq', {
    params, page, limit, enteringTime, attribute
  })
}

export function updateTbRegion(data) {
  return request({
    url: '/tbRegion',
    method: 'put',
    data
  })
}

/**
 * 查册台账接口
 */

 export function updateAudit(data) {
  return request({
    url: '/tbAudit',
    method: 'put',
    data
  })
}


export function addAudit(data) {
  return request({
    url: '/tbAudit',
    method: 'post',
    data
  })
}
// export function getHouseInputInfo() {
//     return request({
//       url: '/tbPlotHousing/select',
//       method: 'get'
//     })
// }

// export function delBuildingInfoById(id) {
//     return deletefn('/api-user/tbBuilding/delete/' + id)
// }

/**
 * 综合信息审批
 */

export function getTbCheckHousing(data, page, limit) {
  console.log('getTbCheckHousing', data, page, limit);
  return request({
    url: `/TbCheckHousing/page_check_housing/${page}/${limit}`,
    method: 'post',
    data
  })
}

export function testMock(title, page, limit) {
  return get('/vue-admin-template/table/list', { title, page, limit })
}

export function mockStandingBookOwn(year, page, limit) {
  return get('/mock/standingBook/standingBook_own', { year, page, limit})
}

export function mockStandingBookCommunity(year, page, limit) {
  return get('/mock/standingBook/standingBook_community', { year, page, limit})
}
export function mockHouseReceive(title, page, limit) {
  return get('/mock/table/house_receive', { title, page, limit })
}

/**
 * 房源建册
 */

// export function getTbHousingCopies (page, limit) {
//   // return request({
//   //   url: '/TbHousingCopies/selectTbHousingCopies',
//   //   method: 'post',
//   //   data
//   // })
//   return get('/TbHousingCopies', {
//     page, limit
//   })
// }

export function getTbHousingCopies(page, limit, data) {
  console.log('getTbHousingCopies1', data);
  return request({
    url: `/TbHousingCopies/page_houses/${page}/${limit}`,
    method: 'post',
    data
  })
}


export function getTbPlotPagePlots(page, limit, data) {
  console.log('getTbHousingCopies1', data);
  return request({
    url: `/tbPlot/pagePlots/${page}/${limit}`,
    method: 'post',
    data
  })
}
/**
 * 房源接收
 */

export function getAccept (params, page, limit) {
  return get('/tbAccept/selectAccept', {
    params,page,limit
  })
}


export function deleteAccept(id) {
  console.log('deleteAccept111', id)
  // return delete('/tbAccept', {
  //   id
  // })
  return request({
    url: '/tbAccept',
    method: 'delete',
    data: {id}
  })
}


/**
 * 房源入库
 * @returns
 *
 */

export function deleteTbCheckHousing(id) {
  return request({
    url: `/TbCheckHousing/delete/${id}`,
    method: 'delete'
  })
}
export function getTbCheckHousingStoredHouse(data, page, limit) {
  console.log('getTbHousingCopies1', data);
  return request({
    url: `/TbCheckHousing/page_stored_house/${page}/${limit}`,
    method: 'post',
    data
  })
}
// 小区信息管理

export function plotMsgList () {
  return get('/', {

  })
}

// export function addAudit(data) {
//   return request({
//     url: '/tbAudit',
//     method: 'post',
//     data
//   })
// }


// export function getAcceptMock (params, page, limit) {
//   return get('/mock/tbAccept/selectAccept', {
//     params, page, limit
//   })
// }

// 机外台账
/**
 *
 */

export function getTbOutdoorAccount (data, page, limit) {
  return request({
    url: `/tb_outdoor_account/page_outdoor_account/${page}/${limit}`,
    method: 'post',
    data
  })
}

// 经营性租赁台账

export function getTbLeaseAccount (page, limit, data) {
  return request({
    url:`/tbLeaseAccount/page_lease_account/${page}/${limit}`,
    method: 'post',
    data
  })
}
