import request from '@/utils/request'
import { get } from '@/utils/request'

// 未审批的修改密码的信息
export function getResetPasswordInfo(page, limit, status) {
    return get('/api-user/tbReset', {
      page,
      limit,
      status
    })
}
export function putResetPasswordInfo(data) {
    return request({
      url: '/api-user/tbReset',
      method: 'put',
      data
    })
  }