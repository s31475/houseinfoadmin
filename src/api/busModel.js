import request from '@/utils/request2'
import qs from 'qs'
import { Login, userInfo, GET_RESOURCE3, SERVICE_ADMIN, SERVICE_BPM } from './api'

/**
 * 加载数据
 * @param data
 */
export function getAllActivity(id) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/a1bpmn/api/model/activity/' + id,
    method: 'GET'
  })
}
/**
 * 获取所有的任务节点以及连线
 * @param data
 */
export function getAllActivityAndSequence(id) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/a1bpmn/api/model/sequence/' + id,
    method: 'GET'
  })
}
/**
 * 加载数据
 * @param data
 */
export function saveModelBus(id, data) {
  return request({
    headers: {
      'Content-Type': 'application/json'

    },
    url: SERVICE_BPM + '/a1bpmn/api/model/bus/' + id,
    method: 'POST',
    data: JSON.stringify(data)

  })
}
/**
 * 加载数据
 * @param data
 */
export function saveModelBusSequence(id, data) {
  return request({
    headers: {
      'Content-Type': 'application/json'

    },
    url: SERVICE_BPM + '/a1bpmn/api/model/sequence/save//' + id,
    method: 'POST',
    data: JSON.stringify(data)

  })
}
/**
 * 获取任务插件
 * @param data
 */
export function saveModelTaskInfo(id, data) {
  return request({
    headers: {
      'Content-Type': 'application/json'

    },
    url: SERVICE_BPM + '/a1bpmn/api/model/urging/save/' + id,
    method: 'POST',
    data: JSON.stringify(data)

  })
}
export function saveModelExpireTaskInfo(id, data) {
  return request({
    headers: {
      'Content-Type': 'application/json'

    },
    url: SERVICE_BPM + '/a1bpmn/api/model/expire/save/' + id,
    method: 'POST',
    data: JSON.stringify(data)

  })
}

/**
 * 获取历史节点
 * @param id
 */
export function getAllHisActivity(id) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/a1bpmn/api/restart/his/instance/' + id,
    method: 'GET'
  })
}
/**
 * 获取模板节点
 * @param id
 */
export function getAllUrgingActivity(id) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/a1bpmn/api/model/urging/' + id,
    method: 'GET'
  })
}
/**
 * 获取到期模板节点
 * @param id
 */
export function getAllExpireActivity(id) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/a1bpmn/api/model/expire/' + id,
    method: 'GET'
  })
}

/**
 * 重启实例
 * @param id
 */
export function restartSave(data) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_BPM + '/a1bpmn/api/cockpit/process-instance/v2/restart',

    method: 'POST',
    data: JSON.stringify(data)
  })
}

