import request from '@/utils/request'

export function menu_delete(id) {
  return request({
    url: `/api-user/menus/delete/${id}`,
    method: 'delete'
  })
}
