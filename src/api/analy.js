import request from '@/utils/request2'
import qs from 'qs'
import { Login, userInfo, GET_RESOURCE3, SERVICE_ADMIN, SERVICE_FORM, SERVICE_BPM } from './api'

export function getFlow(data) {
  return request({
    url: SERVICE_BPM + '/analy/finance/getFlow',
    method: 'post',
    data
  })
}

export function getIncome(data) {
  return request({
    url: SERVICE_BPM + '/analy/income/getData',
    method: 'post',
    data
  })
}
export function getBpmnModelCategory(data) {
  return request({

    url: SERVICE_ADMIN + '/a1bpmn/sys/sysType/v1/getTypesByKey',
    method: 'get'

  })
}

/**
 * 获取待办任务
 * @param data
 */
export function getTaskData(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/a1bpmn/api/runtime/task/v2/list',
    method: 'post',
    data: qs.stringify(data)
  })
}

/**
 * 获取超时任务
 * @param data
 */
export function getTimeOutTaskData(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/a1bpmn/api/runtime/timeout/v2/list',
    method: 'post',
    data: qs.stringify(data)
  })
}

/**
 * 获取所有任务的数据
 * @param data
 */
export function getAllTaskData(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/a1bpmn/api/cockpit/runtime/task/v2/list',
    method: 'post',
    data: qs.stringify(data)
  })
}

export function getHisTaskData(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/a1bpmn/api/history/task/v1/list',
    method: 'post',
    data: qs.stringify(data)
  })
}
export function getInstanceList(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/a1bpmn/api/runtime/instance/v1/getInstanceList',
    method: 'post',
    data: qs.stringify(data)
  })
}
export function getHisInstanceList(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/a1bpmn/api/his/instance/getInstanceList',
    method: 'post',
    data: qs.stringify(data)
  })
}
export function getMessageTemplate(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/a1bpmn/api/messageTemplate/list',
    method: 'POST',
    data: qs.stringify(data)
  })
}
export function getMessageTemplateTask(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/a1bpmn/api/messageTemplate/task',
    method: 'POST',
    data: JSON.stringify(data)

  })
}

export function getMessageTemplateById(id) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/a1bpmn/api/messageTemplate/get/' + id,
    method: 'GET'
  })
}
export function delMessageTemplateById(id) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/a1bpmn/api/messageTemplate/delete/' + id,
    method: 'GET'
  })
}
export function getSql(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/a1bpmn/api/sql/list',
    method: 'POST',
    data: qs.stringify(data)
  })
}
export function getCmdLog(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/api/cmd/log/page/list',
    method: 'POST',
    data: qs.stringify(data)
  })
}
export function getLinkLog(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/api/cmd/data/link/page/list',
    method: 'POST',
    data: qs.stringify(data)
  })
}
export function getMonitor(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/a1bpmn/api/monitor/cache',
    method: 'POST',
    data: qs.stringify(data)
  })
}
export function delMonitorById(data) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_BPM + '/a1bpmn/api/monitor/delete',
    method: 'POST',
    data: JSON.stringify(data)
  })
}
export function getScript(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/a1bpmn/api/runtime/runtime/script/v1/list',
    method: 'POST',
    data: qs.stringify(data)
  })
}
export function getScriptType(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/a1bpmn/api/runtime/runtime/scriptType/v1/list',
    method: 'GET',
    data: qs.stringify(data)
  })
}
export function executeScript(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/a1bpmn/api/runtime/runtime/scriptType/v1/executeScript',
    method: 'POST',
    data: qs.stringify(data)
  })
}
export function saveScript(data) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_BPM + '/a1bpmn/api/runtime/runtime/script/v1/save',
    method: 'POST',
    data: JSON.stringify(data)
  })
}
export function removeScript(data) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_BPM + '/a1bpmn/api/runtime/script/v1/remove',
    method: 'POST',
    data: data
  })
}
export function getScriptById(id) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_BPM + '/a1bpmn/api/runtime/runtime/script/v1/get/' + id,
    method: 'GET'
  })
}
export function getAgentList(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'

    },
    url: SERVICE_BPM + '/a1bpmn/api/agent/list',
    method: 'POST',
    data: qs.stringify(data)
  })
}
export function saveAgent(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'

    },
    url: SERVICE_BPM + '/a1bpmn/api/agent/save',
    method: 'POST',
    data: qs.stringify(data)
  })
}
export function removeAgent(data) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_BPM + '/a1bpmn/api/agent/remove',
    method: 'POST',
    data: data
  })
}
export function getAgent(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/a1bpmn/api/agent/get?id=' + data,
    method: 'get'
  })
}
export function getModelerJson(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/a1bpmn/api/models/listJson',
    method: 'POST',
    data: qs.stringify(data)
  })
}

export function publishModel(id) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/a1bpmn/api/repository/process-definitions/publish?modelId=' + id,
    method: 'get'
  })
}
export function getVersionManagerByKey(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/a1bpmn/api/models/listJson',
    method: 'POST',
    data: qs.stringify(data)
  })
}
export function setModelerVersion(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/a1bpmn/api/model/update',
    method: 'POST',
    data: qs.stringify(data)
  })
}
export function deleteModelerVersion(data) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_BPM + '/a1bpmn/api/models/remove',
    method: 'POST',
    data: data
  })
}
export function renderByTaskId(id) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_FORM + '/form/bus/render/get/' + id,
    method: 'get'
  })
}
export function getTaskBtn(taskId) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_BPM + '/a1bpmn/api/runtime/getTaskBtn/' + taskId,
    method: 'get'
  })
}
export function getNextNode(taskId) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_BPM + '/a1bpmn/api/cockpit/process-instance/getNextNode/' + taskId,
    method: 'get'
  })
}
export function completeTask(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/a1bpmn/api/runtime/task/v2/complete',
    method: 'POST',
    data: qs.stringify(data)
  })
}
export function completeAdHoc(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/a1bpmn/api/runtime/task/completeAdHoc',
    method: 'POST',
    data: qs.stringify(data)
  })
}
export function renderHisForm(processInstanceId) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_FORM + '/form/bus/render/get/renderHisForm/' + processInstanceId,
    method: 'get'
  })
}
export function saveSubTask(data) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_BPM + '/a1bpmn/api/task/pool/save',
    method: 'POST',
    data: data
  })
}

/**
 * 批量转办
 * @param data
 */
export function batchTransfer(data) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_BPM + '/batch/api/transfer',
    method: 'POST',
    data: data
  })
}
/**
 * 批量转交
 * @param data
 */
export function batchHandover(data) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_BPM + '/batch/api/handover',
    method: 'POST',
    data: data
  })
}
/**
 * 批量作废
 * @param data
 */
export function batchInvalid(data) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_BPM + '/batch/api/task/invalid',
    method: 'POST',
    data: data
  })
}

export function getVar(taskId) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_BPM + '/a1bpmn/api/cockpit/getVar/' + taskId,
    method: 'get'
  })
}

/**
 * 根据ID导出
 * @param id
 */
export function exportModelById(id) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_BPM + '/a1bpmn/api/model/export/' + id,
    method: 'get'
  })
}
/**
 * 导入数据
 * @param data
 */
export function uploadSaveModel(data) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_BPM + '/a1bpmn/api/upload/save/model',
    method: 'POST',
    data: JSON.stringify(data)
  })
}

export function getBpmEngine() {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/a1bpmn/api/bpm/engine',
    method: 'GET'
  })
}
export function ipLog(data) {
}

/**
 * 批量完成
 * @param data
 */
export function batchOperate(data) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_BPM + '/batch/api/task/batch/complete',
    method: 'POST',
    data: JSON.stringify(data)
  })
}
/**
 * 任务进度
 * @param data
 */
export function processOperate(taskId) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_BPM + '/a1bpmn/api/runtime/task/info/' + taskId,
    method: 'POST'
  })
}

export function addTask(data) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_BPM + '/a1bpmn/api/runtime/task/add',
    method: 'POST',
    data: data
  })
}

