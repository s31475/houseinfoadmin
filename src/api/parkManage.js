import request from '@/utils/request'

// 园区信息
export function getParkInfoById(id) {
  return get1('/api-user/tbPark/' + id)
}
// 修改密码
export function updatePassword(data) {
  return request({
    url: '/api-user/users/updatePassword',
    method: 'put',
    data
  })
}

// 修改园区信息
export function putParkInfo(data) {
  return request({
    url: '/api-user/tbPark/updateTbPark',
    method: 'put',
    data
  })
}
