import request from '@/utils/request2'
import qs from 'qs'
import { Login, userInfo, GET_RESOURCE3, SERVICE_ADMIN, SERVICE_FORM, SERVICE_BPM } from './api'

export function getRunCommuList(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'

    },
    url: SERVICE_BPM + '/a1bpmn/api/commu/list',
    method: 'POST',
    data: qs.stringify(data)
  })
}
export function getHisCommuList(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'

    },
    url: SERVICE_BPM + '/a1bpmn/api/commu/his/list',
    method: 'POST',
    data: qs.stringify(data)
  })
}

export function tagRead(data) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_BPM + '/a1bpmn/api/commu/tagRead',
    method: 'POST',
    data: data
  })
}

