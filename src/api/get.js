import request from '@/utils/request'

export function menu_findAlls() {
  return request({
    url: '/api-user/menus/newCurrent',
    method: 'get'
  })
}

export function saveOrUpdate(data) {
  return request({
    url: '/api-user/menus/saveOrUpdate',
    method: 'post',
    data
  })
}
