import request from '@/utils/request2'
import qs from 'qs'
import { Login, userInfo, GET_RESOURCE3, SERVICE_ADMIN, SERVICE_FORM, SERVICE_BPM } from './api'

export function getStartUpRuningList(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'

    },
    url: SERVICE_BPM + '/a1bpmn/api/startUp/list/running',
    method: 'POST',
    data: qs.stringify(data)
  })
}
export function getStartUpHisList(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'

    },
    url: SERVICE_BPM + '/a1bpmn/api/startUp/his/list',
    method: 'POST',
    data: qs.stringify(data)
  })
}

