import request from '@/utils/request2'
import qs from 'qs'
import { Login, userInfo, GET_RESOURCE3, SERVICE_ADMIN, SERVICE_BPM } from './api'

// /api/runtime/task/v1/informed
/**
 * 传阅
 * @param data
 */
export function informedTask(data) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_BPM + '/a1bpmn/api/cockpit/processInstance/v2/informed',
    method: 'POST',
    data: JSON.stringify(data)
  })
}

/**
 * 转办
 * @param data
 */
export function transferTask(data) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_BPM + '/a1bpmn/api/runtime/task/v2/transferTask',
    method: 'POST',
    data: JSON.stringify(data)
  })
}

/**
 * 作废
 * @param id
 */
export function invalidTask(id) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_BPM + '/a1bpmn/api/history/task/v1/invalid/' + id,
    method: 'get'
  })
}
/**
 * 作废流程实例
 * @param id
 */
export function invalidProcessInstance(processInstanceId) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_BPM + '/a1bpmn/api/cockpit/runtime/v2/invalid/' + processInstanceId,
    method: 'get'
  })
}

/**
 * 历史意见
 * @param id
 */
export function opinionTask(id) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_BPM + '/a1bpmn/api/runtime/hisInstance/v1/nodeOpinion?instId=' + id,
    method: 'get'
  })
}

/**
 * 特事特办节点
 * @param id
 */
export function getTransactionUrge(id) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_BPM + '/a1bpmn/api/runtime/task/v1/getTransactionUrge/' + id,
    method: 'get'
  })
}
/**
 * 特事特办
 * @param id
 */
export function changeActivity(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/a1bpmn/api/cockpit/task/v1/anyway/changeActivity/' + data.taskId,
    method: 'POST',
    data: qs.stringify(data)
  })
}
export function getOutgoingFlows(id) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/a1bpmn/api/cockpit/process-instance/outgoingFlows/' + id,
    method: 'get'
  })
}
export function addNodeOperate(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/a1bpmn/api/cockpit/process-instance/addNode/' + data.taskId,
    method: 'POST',
    data: qs.stringify(data)
  })
}
export function addSignOperate(data) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_BPM + '/a1bpmn/api/cockpit/process-instance/v2/addSign/' + data.taskId,
    method: 'POST',
    data: JSON.stringify(data)
  })
}

/**
 * 保存
 * @param data
 */
export function saveform(data) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_BPM + '/a1bpmn/api/runtime/task/save',
    method: 'POST',
    data: JSON.stringify(data)
  })
}
export function getBackNode(taskId) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_BPM + '/a1bpmn/api/runtime/task/v1/getBackNode/' + taskId,
    method: 'GET'
  })
}
export function getBackNodeUser(taskId, nodeId) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_BPM + '/a1bpmn/api/runtime/task/v1/getBackNodeUser/' + taskId + '/' + nodeId,
    method: 'GET'
  })
}

