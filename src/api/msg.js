import request from '@/utils/request2'
import qs from 'qs'
import { Login, userInfo, GET_RESOURCE3, SERVICE_ADMIN, SERVICE_FORM, SERVICE_BPM } from './api'

export function getUnReadList(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'

    },
    url: SERVICE_BPM + '/a1bpmn/api/notify/unRead/list',
    method: 'POST',
    data: qs.stringify(data)
  })
}
export function getReadList(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'

    },
    url: SERVICE_BPM + '/a1bpmn/api/notify/read/list',
    method: 'POST',
    data: qs.stringify(data)
  })
}
export function getAllMsgType() {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'

    },
    url: SERVICE_BPM + '/a1bpmn/api/notify/type/All',
    method: 'GET'
  })
}
export function tagReadMsg(data) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_BPM + '/a1bpmn/api/notify/tagRead',
    method: 'POST',
    data: data
  })
}

