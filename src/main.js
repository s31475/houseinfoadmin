import Vue from 'vue'

// v4设计器
import nodeWrap from '@/components/nodeWrap'
Vue.use(nodeWrap)
Vue.component('nodeWrap', nodeWrap) // 初始化组件
import addNode from '@/components/addNode'
Vue.use(addNode)
Vue.component('addNode', addNode) // 初始化组件

import VueCodemirror from 'vue-codemirror'
import 'codemirror/lib/codemirror.css'
Vue.use(VueCodemirror)

import '../static/ueditor/ueditor.config.js'
import '../static/ueditor/ueditor.all.min.js'
import '../static/ueditor/lang/zh-cn/zh-cn.js'
// import '../static/ueditor/ueditor.parse.min.js'
import '../static/ueditor/themes/default/css/ueditor.css'

import iView from 'view-design'
import 'view-design/dist/styles/iview.css'
Vue.use(iView)

//表单相关的
import FormMaking from './views/form/index'
Vue.use(FormMaking)


import 'normalize.css/normalize.css' // A modern alternative to CSS resets

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import locale from 'element-ui/lib/locale/lang/en' // lang i18n

import '@/styles/index.scss' // global css

import search from '@/assets/js/search'
Vue.prototype.$search = search

import App from './App'
import store from './store'
import router from './router'

import '@/icons' // icon
import '@/permission' // permission control

import 'xe-utils' // vxe-table依赖xe-utils
import VXETable from 'vxe-table' // vxe-table
import 'vxe-table/lib/style.css'

// import "../mock/index";

Vue.use(VXETable)
import '@/utils/validate'
Vue.prototype.$XModal = VXETable.modal

/**
 * If you don't want to use mock-server
 * you want to use MockJs for mock api
 * you can execute: mockXHR()
 *
 * Currently MockJs will be used in the production environment,
 * please remove it before going online ! ! !
 */
// if (process.env.NODE_ENV === 'production') {
//   const { mockXHR } = require('../mock')
//   mockXHR()
// }

if (process.env.NODE_ENV === 'development') {
  const { mockXHR } = require('../mock')
  mockXHR()
}

// set ElementUI lang to EN
Vue.use(ElementUI, { locale })
// 如果想要中文版 element-ui，按如下方式声明
// Vue.use(ElementUI)

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
