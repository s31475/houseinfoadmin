const Mock = require('mockjs')

const data1 = Mock.mock({
  'items|23': [{
    initiator: '@sentence(1, 5)',
    title: '@sentence(1, 10)',
    createTime: '@datetime',
    flowStatus: '@sentence(1, 5)',
    upId: '@id',
    nextId: '@id',
    process: '@sentence(1, 5)',
    dealTime: '@datetime',
    'title|1': [1, 2, 3]
  }]
})

const data2 = Mock.mock({
  'items|23': [{
    id: '@id',
    title: '@sentence(1, 10)',
    initiator: '@sentence(1, 5)',
    receiveCount: '@datetime',
    receiveArea: '@datetime',
    paper: '@datetime',
    receiveTime: '@datetime',
  }]
})


const data3 = Mock.mock({
  'items|23': [{
    'id|+1': 1,
    attribute: '商业',
    plotName: '随机面积',
    totalArea: '随机总面积',
    paper: '随机总数',
    remark: '@sentence(1, 5)',
  }]
})

const data4 = Mock.mock({
  'items|23': [{
    id: '@id',
    purpose: '商业',
    area: '随机面积',
    totalArea: '随机总面积',
    total: '随机总数',
    remark: '@sentence(1, 5)',
  }]
})
module.exports = [
  {
    url: '/vue-admin-template/table/list',
    type: 'get',
    response: config => {
      const items = data1.items
      return {
        code: 0,
        data: {
          current: 1,
          pages: 0,
          records: items,
          searchCount: true,
          size: 10,
          total: items.length
        },
        msg: "执行成功"
      }
    }
  },
  {
    url: '/mock/table/house_receive',
    type: '',
    response: config => {
      const items = data2.items
      return {
        code: 0,
        data: {
          current: 1,
          pages: 0,
          records: items,
          searchCount: true,
          size: 10,
          total: items.length
        },
        msg: "执行成功"
      }
    }
  },
  {
    url: '/mock/standingBook/standingBook_own',
    type: '',
    response: config => {
      const items = data3.items
      return {
        code: 0,
        data: items,
        msg: "执行成功"
      }
    }
  },
  {
    url: '/mock/standingBook/standingBook_community',
    type: '',
    response: config => {
      const items = data4.items
      return {
        code: 0,
        data: items,
        msg: '执行成功'
      }
    }
  }
]
